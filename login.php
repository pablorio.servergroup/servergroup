<?php include('db.php');?>
<?php include('includes/header.php');?>
<div class="row">
    <div class="col-sm-4 mx-auto">
        <?php 
       

        if(isset($_SESSION['message'])){

            if($_SESSION['message'] != false) {

              ?>

        <div class="alert alert-info alert-dismissible fade show" role="alert">
            <p style="text-align: center;"><?php echo $_SESSION['message'] ;?></p>
            <!--button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button-->
        </div>
        <?php  
            }
        }
        $_SESSION['message'] = false;
        ?>
    </div>
</div>
<div id="login" style="margin-bottom: 10%; margin-top:5%;">
    <div class="row mt-5">
        <div class="col-sm-2 mx-auto">
            <img src="assets/img/clients/client-1.png" class="img-fluid" alt="">
        </div>
    </div>
    <div class="container align-items-center mt-5">
        <div class="card col-sm-4 mx-auto">
            <div class="card-body">
                <form action="loginboton.php" method="post">
                    <div class="row">
                        <div class="form-group col-md-10 mx-auto">
                            <label for="name">Usuario</label>
                            <input type="text" name="user" class="form-control" id="name" data-rule="minlen:4"
                                data-msg="Please enter at least 4 chars" />
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-10 mx-auto">
                            <label for="name">Pass</label>
                            <input type="password" class="form-control" name="pass" id="pass" />
                            <div class="validate"></div>
                        </div>
                    </div>
                    <br>
                    <div class="text-center">
                        <button class="btn btn-primary" name="login" type="submit">Login</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php');?>