<?php

$conn = mysqli_connect(
    '31.170.161.22', // 'localhost'
    'u101685278_servergroup', //'root'
    'Pachiman2020', // ''
    'u101685278_servergroup' //'nombrebase'
) or die(mysqli_erro($mysqli));

session_start();

?>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">


</head>

<body>
    <div class="container mt-5">
        <div class="row pb-3 mt-2">
            <div class="col-lg-5 mx-auto">
                <img src="assets\img\logos\logo-portada-gr.png" class="img-fluid" alt="">
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="card-title"> 
                    <h4 style="text-align: center;"> <strong> Formulario | </strong><strong style="color:#f30e0e;">Solicitud de Cobertura</strong> </h4>
                </div>
            </div>
            <div class="card-body">
                <form action="prueba.php" method="POST" id="form_insert" enctype="multipart/form-data">
                    <div class="row justify-content-center mt-2">
                        <div class="col-md-5">
                            <label for="text">Operación</label>
                            <select class="form-control" name="operacion[]" id="" required>
                                <option value="">-.Elegir.-</option>
                                <option value="Maritimo">Marítimo</option>
                                <option value="Terrestre - Marítimo">Terrestre - Marítimo</option>
                                <option value="Terrestre">Terrestre</option>
                                <option value="Aereo">Aéreo</option>
                                <option value="Terrestre - Aereo">Terrestre - Aéreo</option>
                            </select>
                        </div>
                        <div class="col-md-5">
                            <label for="date">Fecha de Salida</label>
                            <input type="date" class="form-control" name='fecha'
                                placeholder="Descripción corta del Problema">
                        </div>
                    </div>
                    <div class="row justify-content-center mt-2">
                        <div class="col-md-5">
                            <label for="text">Origen</label>
                            <input type="text" class="form-control" name="origen" placeholder="Mendoza, Argentina"
                                required>
                            <small><i>Origen: desde donde se quiere asegurar la carga.</i></small>
                        </div>
                        <div class="col-md-5">
                            <label for="text">Destino</label>
                            <input type="text" class="form-control" name="destino" placeholder="Okland, USA" required>
                            <small><i>Destino: hasta donde se quiere asegurar la carga.</i></small>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-1"></div>
                        <div class="col-md-5">
                            <label for="prioridad">Invoice</label>
                            <input type="text" class="form-control" name="invoice" placeholder="E0001-00004535">
                        </div>
                        <div class="col-md-5">
                            <label for="prioridad">Facturar a:</label>
                            <input type="text" class="form-control" name="facturacion" placeholder="Su Compañia SA">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-1"></div>
                        <div class="col-md-10">
                            <label for="prioridad">Destinatario de Certificado</label>
                            <input type="text" class="form-control" name="destinatario" placeholder="john@empresa.com">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-1"></div>
                        <div class="col-md-10">
                            <label for="prioridad">Breve descripción de la Mercaderia/Carga</label>
                            <input type="text" class="form-control" name="descripcion" placeholder="Ajo en bins">
                        </div>
                    </div>
                    <br>
                    <div class="row mt-2">
                        <div class="col-sm-1"></div>
                        <div class="col-md-10">
                            <label for="prioridad">Monto a asegurar:</label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="text" class="pt-2">Valor de Carga</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" name="montoVC" placeholder="200.00">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="text" class="pt-2">Flete</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" name="montoF" placeholder="200.00">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="text" class="pt-2">Ben.Imaginario</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" name="montoBI" placeholder="200.00">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="text" class="pt-2">Derechos</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" name="montoD" placeholder="200.00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-1"></div>
                        <div class="col-md-10">
                            <label for="prioridad">Beneficiario</label>
                            <input type="text" class="form-control" name="beneficiario"
                                placeholder="Empresa Exportadora SA">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-6">
                            <label fot="archivo">Adjuntar Factura/BL/Booking:</label>
                            <input type="file" id="file-input" name="document_invoice" class="form-control-file">
                        </div>
                    </div>
                    <div class="row m-3 ">
                        <button type="submit" name="enviar" class="btn btn-primary col-sm-2 mx-auto">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <br>
    <?php

        $mail = "";

        if (isset($_POST['enviar'])) {

            foreach ($_POST['operacion'] as $operacion);
            $fecha = $_POST['fecha'];
            $origen = $_POST['origen'];
            $destino = $_POST['destino'];
            $invoice = $_POST['invoice'];
            $descripcion = $_POST['descripcion'];
            $montoVC = $_POST['montoVC'];
            $montoF = $_POST['montoF'];
            $montoBI = $_POST['montoBI'];
            $montoD = $_POST['montoD'];
            $beneficiario = $_POST['beneficiario'];
            $facturacion = $_POST['facturacion'];
            $destinatario = $_POST['destinatario'];
            $total = $montoBI + $montoD + $montoF + $montoVC;

            $nombre_invoice = $_FILES['document_invoice']['name'];
            $guardar_invoice = $_FILES['document_invoice']['tmp_name'];
            $folder = 'documentos';

            move_uploaded_file( $guardar_invoice,'documentos/'.$nombre_invoice);

            
            $to = 'prio@servergroupsa.com, seguridad@servergroupsa.com, certificados@servergroupsa.com';

            //remitente del correo
            $from = 'prio@servergroupsa.com';
            $fromName = 'Pagina Server Group';

                        
            //Asunto del email
            $subject = 'Nuevo Pedido de Certificado :: ' . $beneficiario;

            //Ruta del archivo adjunto
            $file_ad = 'documentos/'. $nombre_invoice;

            //Contenido del Email
            $htmlContent = 
            '<head>

            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
            <style>
                body{
                    font-family: "Baloo 2", cursive";
                }
            </style>
            <body>
            <h4 style="color:#2E303E;"> Estimado Nacho:</h4>
            <br>
            <p> Por favor emirtir el siguiente certificado:</p>
            <br>
            <p><strong>Operación: <strong>' . $operacion . '</p>
            <p><strong>Fecha de Salida: <strong>' . $fecha . '</p>
            <p><strong>Desde: <strong>' . $origen . '</p>
            <p><strong>Hasta: <strong>' . $destino . '</p>
            <p><strong>Invoice: <strong>' . $invoice . '</p>
            <p><strong>Descripción: <strong>' . $descripcion . '</p>
            <p><strong>Monto Valor Carga: <strong>' . $montoVC . '</p>
            <p><strong>Monto Flete: <strong>' . $montoF . '</p>
            <p><strong>Monto Ben. Img.: <strong>' . $montoBI . '</p>
            <p><strong>Monto Der.: <strong>' . $montoD . '</p>
            <p><strong>Beneficiario: <strong>' . $beneficiario . '</p>
            <p><strong>Facturar a: <strong>' . $facturacion . '</p>
            <p><strong>Total a Asegurar: <strong>' . $total. '</p>
            <br>  
            <p><strong>Enviar Certificado a: <strong>' . $destinatario. '</p>


            <br>
            <br>
            <br>
            <p style="text-align:center; color:#2E303E">Tecnología de <a style="color:#17A589;" href="http://builditdesing.com" >BUILD.IT</a> utilizada por Picadas Macanudas :: Sabores que compartimos.</p>
            </body>
            </html>';
            
            //Encabezado para información del remitente
            $headers = "De: $fromName" . " <" . $from . ">";

            
            //Limite Email
            $semi_rand = md5(time());
            $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";


            //Encabezados para archivo adjunto
            $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
            
            //límite multiparte
            $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
                "Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";

            //preparación de archivo
            if(!empty($file_ad)){
                if(is_file($file_ad)){
                    $message .= "--{$mime_boundary}\n";
                    $fp =    @fopen($file_ad,"rb");
                    $data =  @fread($fp,filesize($file_ad));
                    $i = 0;
                    @fclose($fp);
                    $data = chunk_split(base64_encode($data));
                    $message .= "Content-Type: application/octet-stream; name=\"".basename($file_ad)."\"\n" . 
                    "Content-Description: ".basename($file_ad[$i])."\n" .
                    "Content-Disposition: attachment;\n" . " filename=\"".basename($file_ad)."\"; size=".filesize($file_ad).";\n" . 
                    "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
                }
            }
            $message .= "--{$mime_boundary}--";
            $returnpath = "-f" . $from;

            //Enviar EMail
                $mail = @mail($to, $subject, $message, $headers, $returnpath); 


        if ($mail) {

            $_SESSION['message'] = 'Su Solicitud se envió con exito. En breve resibirá por correo el certificado de Cobertura';
            $_SESSION['message_type'] = 'info';
            echo "<script> window.location='info.php'; </script>";
                        


        } else {

            $_SESSION['message'] = 'Reintente nuevamente: su solicitd no fue enviada!';
            $_SESSION['message_type'] = 'warning';
            header('location: info.php');
            echo "<script> window.location='info.php'; </script>";



        }
    }
?>

    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>