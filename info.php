<?php

$conn = mysqli_connect(
    '31.170.161.22', // 'localhost'
    'u101685278_servergroup', //'root'
    'Pachiman2020', // ''
    'u101685278_servergroup' //'nombrebase'
) or die(mysqli_erro($mysqli));

session_start();

?>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">


</head>

<body>
    <div class="container mt-5">
        <div class="row pb-3 mt-2">
            <div class="col-lg-5 mx-auto">
                <img src="assets\img\logos\logo-portada-gr.png" class="img-fluid" alt="">
            </div>
        </div>
        <?php 
                    if(isset($_SESSION['message'])){

                        if($_SESSION['message'] != false) {

                ?>



        <div class="card border-<?php echo $_SESSION['message']?>">
            <div class="card-header">
                Estado de la Solicitud!
            </div>
            <div class="card-body">
                <p class="card-text"><?php echo $_SESSION['message'] ;?></p>
                <a href="prueba.php" class="btn btn-info">Volver</a>
            </div>
        </div>
    </div>
    <br>
    <?php  
            }
        }
        
        $_SESSION['message'] = false;
        ?>


    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>