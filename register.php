<?php include('db.php');?>
<?php include('includes/header.php');?>
<div class="row">
    <div class="col-sm-4 mx-auto">
        <?php 

        $padding = '';
        $paddinginner = 'mt-2';

        if(isset($_SESSION['message'])){

            if($_SESSION['message'] != false) {
                  
                $padding= 'pt-0';
                $paddinginner= 'pt-0';?>

        <div class="alert alert-danger alert-dismissible fade show"
            style="z-index: 1031; margin-top: 7rem; text-align:center;" role="alert">
            <?php echo $_SESSION['message'] ;?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php  
            }
        }
        $_SESSION['message'] = false;
        ?>
    </div>
</div>
<div id="login" style="margin-bottom: 10%; margin-top:5%;">
    <div class="row mt-5">
        <div class="col-sm-2 mx-auto">
            <img src="assets/img/clients/client-1.png" class="img-fluid" alt="">
        </div>
    </div>
    <div class="container align-items-center mt-5">
        <div class="card col-sm-4 mx-auto">
            <div class="card-body">
                <form method='POST' action="registerboton.php">
                    <div class="row">
                        <div class="form-group col-md-10 mx-auto">
                            <label>Usuario</label>
                            <input type="user" class="form-control" name="user" placeholder="User Name"
                                data-rule="minlen:4" data-msg="Please enter at least 4 chars" required>
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-10 mx-auto">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-10 mx-auto">
                            <label>Pass</label>

                            <input type="password" class="form-control" name="pass" id="pass" placeholder="Password" />
                            <div class="validate"></div>
                        </div>
                    </div>
                    <br>
                    <div class="text-center">
                        <button class="btn btn-primary" name="register" type="submit">Registrar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php');?>