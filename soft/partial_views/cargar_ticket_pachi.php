<div class="container mt-5">
    <br>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 style="text-align: center;">Cargar nuevo Ticket</h3>
            </div>
        </div>
        <div class="card-body">
            <form action="../actions/cargar_ticket.php" method="POST" id="form_insert" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <label for="text">Título</label>
                        <input type="text" class="form-control" name='titulo'
                            placeholder="Descripción corta del Problema" required>
                    </div>
                </div>
                <div class="row justify-content-center mt-2">
                    <div class="col-md-3">
                        <label for="date">Fecha del Pedido</label>
                        <input type="date" class="form-control" name='fecha'
                            placeholder="Descripción corta del Problema">
                    </div>
                    <div class="col-md-3">
                        <label for="prioridad">Prioridad</label>
                        <select class="form-control" name="prioridad[]" id="" required>
                            <option value="">-.Elegir.-</option>
                            <option value="Baja">Baja</option>
                            <option value="Media">Media</option>
                            <option value="Alta">Alta</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-6 mx-auto">
                        <label for="text">Url</label>
                        <input type="text" class="form-control" name='url' placeholder="[link del problema]">
                        <small>ej https://www.taker.com.ar/taker_ap_barrios/panel</small>
                    </div>
                </div>
                <div class="row justify-content-center mt-2">
                    <div class="col-md-3">
                        <label for="date">Aplicación</label>
                        <select name="app[]" class="form-control" required>
                            <option value="">-.Elegir.-</option>
                            <option value="Taker AP">Taker AP</option>
                            <option value="Taker RCI">Taker RCI</option>
                            <option value="Taker T">Taker T</option>
                            <option value="Midas">Midas</option>
                            <option value="Facturación">Facturación</option>
                            <option value="Otros">Otros</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="prioridad">Tipo</label>
                        <select class="form-control" name="type[]" id="" required>
                            <option value="">-.Elegir.-</option>
                            <option value="Error">Error</option>
                            <option value="Mejora">Mejora</option>
                            <option value="Nueva Función">Nueva Función</option>
                            <option value="Idea">Idea</option>
                            <option value="Otro">Otro</option>
                        </select>
                    </div>
                </div>
                <br>
                <div class="row justify-content-center mt-2">
                    <div class="col-sm-6">
                        <label fot="archivo">Adjuntar Archivo:</label>
                        <input type="file" id="file-input" name="document_ticket" class="form-control-file">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <textarea class="form-control mx-auto" name="descripcion" id="" cols="20" rows="10"
                            placeholder="Detalle del Problema" required></textarea>
                    </div>
                </div>
                <br>
                <div class="row">
                    <button type="submit" name="enviar" class="btn btn-primary col-sm-2 mx-auto">Enviar</button>
                </div>
        </div>
        </form>
    </div>
</div>