<div class="container">
    <br>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 style="text-align: center; font-family:'Exo 2',sans-serif;">Historial de Tickets</h3>
            </div>
        </div>
        <div class="card-body">
            <div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite"></div>
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Titulo</th>
                        <th>Tipo</th>
                        <th>Status</th>
                        <th>User</th>
                        <th>Prioridad</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                   
                $user = $_SESSION['user'];
                $query = "SELECT * FROM `tickets` WHERE `app` = 'Taker RCI' ORDER BY `priority` ASC" ;   
                $result_tasks = mysqli_query($conn, $query);     
                    
                    while($row = mysqli_fetch_assoc($result_tasks)) { 
                        
                        $id =$row['id']; 
                        $titulo =$row['titulo']; 
                        $description = $row['description']; 
                        $app = $row['app'];
                        $url = $row['url'];
                        $type = $row['type'];
                        $priority = $row['priority'];
                        $respuesta = $row['respuesta'];
                        $data_toma = $row['data_toma'];
                        $adjunto = $row['adjunto'];
                        $status_general = $row['status_general'];
                        $usuario = $row ['user'];
                        $created_at = $row['created_at'];
                        ?>
                    <tr>
                        <td><?php echo $id; ?></td>
                        <td><?php echo $titulo; ?></td>
                        <td><?php echo $type; ?></td>
                        <td><?php echo $status_general; ?></td>
                        <td><?php echo $usuario; ?></td>
                        <td><span class="badge badge-info"><?php echo $priority; ?></span></td>

                        <td style="text-align:center;">
                            <a title="Editar Cnee" data-toggle="modal" data-target="#editar<?php echo $id;?>"
                                style="color: #17A589; padding:2%; ">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a title="Ver Cnee" data-toggle="modal" data-target="#ver<?php echo $id;?>"
                                style="color: #17A589; padding:2%; ">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a title="Eliminar" href="../actions/delete_ticket.php?id=<?php echo $id;?>"
                                style="color: #17A589; padding:2%; ">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    
                    <!--|MODAL EDITAR|-->
                    <div class="modal fade" id="editar<?php echo $id;?>" tabindex="-1" role="dialog"
                        aria-labelledby="scrollmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 style="text-align:center;" class="modal-title" id="scrollmodalLabel">
                                        Detalles
                                        <strong><?php echo $titulo; ?></strong>
                                        <span class="badge badge-primary"><?php echo $priority;?></span>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <form action="../actions/editar_ticket.php?id=<?php echo $id;?>" method="POST"
                                        id="form_insert" enctype="multipart/form-data">
                                        <div class="card col-sm-10 mx-auto">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-12 mx-auto">
                                                        <input class="form-control" type="text"
                                                            value="<?php echo $titulo;?>" name="titulo" required>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <input type="file" id="file-input" name="document_ticket"
                                                            class="form-control-file">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-12 mx-auto">
                                                        Descripcion:
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 mx-auto">
                                                        <textarea class="form-control" name="description" id=""
                                                            cols="30" rows="10"><?php echo $description;?></textarea>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row form-group ">
                                                    <div class="col-sm-4 mx-auto">
                                                        <button type="submit" class="btn btn-primary mr-2"
                                                            name="editar">Editar</button>
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Cerrar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--|FINAL MODAL EDITAR|-->
                    <!--|MODAL VER|-->
                    <div class="modal fade" id="ver<?php echo $id;?>" tabindex="-1" role="dialog"
                        aria-labelledby="scrollmodalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 style="text-align:center;" class="modal-title" id="scrollmodalLabel">
                                        Detalles
                                        <strong><?php echo $titulo; ?></strong>
                                        <span class="badge badge-primary"><?php echo $priority;?></span>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <div class="card col-sm-10 mx-auto">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12 mx-auto">
                                                    <div class="alert alert-primary" role="alert">
                                                        <?php echo $titulo . ' - ' .$app . ' - ['.$created_at.']';?>
                                                        <br>
                                                        <a href="<?php echo $url;?>"
                                                            target="_blank"><small><?php echo $url;?></small></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <p>Adjunto:<a class=" col-sm-1 fa fa-paperclip"
                                                            title="<?php echo $adjunto; ?>" target="_blank"
                                                            href="../actions/archivos_tickets/<?php echo $id . '/'.$adjunto; ?>">
                                                        </a>
                                                        <small><?php echo $adjunto;?></small>
                                                    </p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="alert alert-light pt-0" role="alert">
                                                        Este tickets esta: <?php echo $status_general;?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-10 mx-auto">
                                                        Descripcion:
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-10 mx-auto">
                                                        <!-- border border-secondary rounded -->
                                                        <p><?php echo $description;?></p>
                                                        <hr>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-10 mx-auto">
                                                        Respuesta:
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-10 mx-auto">
                                                        <!-- border border-secondary rounded -->
                                                        <p><?php echo $respuesta;?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group ">
                                                <div class="col-sm-2 mx-auto">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--|FINAL MODAL VER|-->
                    <?php }   ?>
                </tbody>
            </table>
        </div>
    </div>
</div>