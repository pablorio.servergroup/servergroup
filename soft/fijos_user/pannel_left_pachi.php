 <!-- Left Panel -->
 <aside id="left-panel" class="left-panel">
     <nav class="navbar navbar-expand-sm navbar-default">
         <div id="main-menu" class="main-menu collapse navbar-collapse">
             <ul class="nav navbar-nav">
                 <li>
                     <a href="principal.php"><i class="menu-icon fa fa-laptop"></i>Incio</a>
                 </li>
                 <li>
                     <a href="cargar_ticket.php"><i class="menu-icon fa fa-laptop"></i>Nuevo Ticket</a>
                 </li>
                 <!--li>
                     <a href="../views/picadas_principal.php"><i class="menu-icon fa fa-laptop"></i>Marcar Horas</a>
                 </li-->
                 <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                         aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tickets</a>
                     <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-check-square-o"></i><a href="tickets_super_user_control_taker_ap.php">Taker AP</a></li>
                        <li><i class="fa fa-check-square-o"></i><a href="tickets_super_user_control_taker_rci.php">Taker RCI</a></li>
                        <li><i class="fa fa-check-square-o"></i><a href="tickets_super_user_control_taker_t.php">Taker T</a></li>
                        <li><i class="fa fa-check-square-o"></i><a href="tickets_super_user_control_midas.php">Midas</a></li>
                        <li><i class="fa fa-check-square-o"></i><a href="tickets_super_user_control_taker_facturacion.php">Facturación</a></li>
                        <li><i class="fa fa-check-square-o"></i><a href="tickets_super_user_control_taker_otros.php">Otros</a></li>

                         
                     </ul>
                 </li>
                 <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                         aria-expanded="false"> <i class="menu-icon fa fa-truck"></i>Sprin Actual</a>
                     <ul class="sub-menu children dropdown-menu">
                         <li><i class="fa fa-edit"></i><a href="../views/tabla_fletes.php">Aconcagua</a></li>
                         <li><i class="fa fa-map-marker"></i><a href="../views/tabla_localidades.php">San Juan</a>
                         </li>
                     </ul>
                 </li>

                 <li>
                     <a href="../ayuda/ayuda.php"><i class="menu-icon ti-headphone-alt"></i>Ayuda</a>
                 </li>
             </ul>
         </div><!-- /.navbar-collapse -->
     </nav>
 </aside><!-- /#left-panel -->

 <!-- Gestion de Tickets:
 - Taker AP
 - Taker RCI
 - Taker T
 - Midas
 - Facturacion 
 - Otros

Gestion de Usuarios. 

Reunion Funcionales. Traducción de la Reunión. 
SCRUM.- -->