 <!-- Left Panel -->

 <aside id="left-panel" class="left-panel">
     <nav class="navbar navbar-expand-sm navbar-default">
         <div id="main-menu" class="main-menu collapse navbar-collapse">
             <ul class="nav navbar-nav">
                 <li>
                     <a href="../views/picadas_principal.php"><i class="menu-icon fa fa-laptop"></i>Inicio <?php echo $_SESSION['permiso'];?></a>
                 </li>
                 <li>
                     <a href="../views/picadas_principal.php"><i class="menu-icon fa fa-laptop"></i>Nuevo Ticket</a>
                 </li>
                 <li>
                     <a href="../views/picadas_principal.php"><i class="menu-icon fa fa-laptop"></i>Marcar Horas</a>
                 </li>
                 <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                         aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tickets</a>
                     <ul class="sub-menu children dropdown-menu">
                         <li><i class="fa fa-check-square-o"></i><a href="../views/stock_picadas.php">Pendientes</a>
                         </li>
                         <li><i class="fa fa-beer"></i><a href="../views/proveedores_bebidas.php">Enviados a Aconcagua</a>
                         </li>
                         <li><i class="fa fa-coffee"></i><a href="../views/proveedores_dulce.php">Enviado a San Juan</a>
                         </li>
                         <li><i class="fa fa-lemon-o"></i><a href="../views/proveedores_chipa.php">Antiguos</a>
                         </li>
                         <li><i class="fa fa-shopping-cart"></i><a href="../views/proveedores_insumos.php">Prioridad Alta</a>
                         </li>
                     </ul>
                 </li>
                 <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                         aria-expanded="false"> <i class="menu-icon fa fa-truck"></i>Sprin Actual</a>
                     <ul class="sub-menu children dropdown-menu">
                         <li><i class="fa fa-edit"></i><a href="../views/tabla_fletes.php">Aconcagua</a></li>
                         <li><i class="fa fa-map-marker"></i><a href="../views/tabla_localidades.php">San Juan</a>
                         </li>
                     </ul>
                 </li>
                
                 <li>
                     <a href="../ayuda/ayuda.php"><i class="menu-icon ti-headphone-alt"></i>Ayuda</a>
                 </li>
             </ul>
         </div><!-- /.navbar-collapse -->
     </nav>
 </aside><!-- /#left-panel -->