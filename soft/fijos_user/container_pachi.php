<div class="container-fluid">
    <?php  $querysum = "SELECT SUM(horas) FROM `horas` WHERE MONTH(`fecha`)=MONTH(NOW())";
                            $resultsum = mysqli_query($conn, $querysum); 
                            
                            if($resultsum){
                                $row = mysqli_fetch_assoc($resultsum);
                                $horas = $row['SUM(horas)'];
                                $total = $horas * 650;
                            };  ?>
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="card mt-2">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib">
                            <i class="ti-money text-success border-success"></i>
                        </div>
                        <div class="stat-content dib">
                            <div class="stat-text">Profit Mes</div>
                            <div class="stat-digit">$ <?php echo  $total;?></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card mt-2">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib">
                            <i class="ti-alarm-clock text-primary border-primary"></i>
                        </div>
                        <div class="stat-content dib">
                            <div class="stat-text">Horas Mes</div>
                            <div class="stat-digit"><?php echo  $horas;?></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 style="text-align:center;">Horas Semanales</h3>
        </div>
        <div class="card-body">
            <div class="table-reponsive dataTables_info" id="bootstrap-data-table_info" role="status"
                aria-live="polite">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead style="text-align:center;">
                        <tr>
                            <th>Fecha</th>
                            <th>Titulo</th>
                            <th>hr</th>
                            <th>Tipo</th>
                            <th>Empresa</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                           
                            $query = "SELECT * FROM `horas`  WHERE YEARWEEK(fecha)=YEARWEEK(NOW());";
                            $result = mysqli_query($conn, $query);                             
                            while($row = mysqli_fetch_assoc($result)) { ?>
                        <tr>
                            <td><?php echo $row['fecha']; ?></td>
                            <td><?php echo $row['titulo']; ?></td>
                            <td><?php echo $row['horas']; ?></td>
                            <td><?php echo $row['colaborador'];?></td>
                            <td><?php echo $row['empresa']; ?></td>
                            <td style=" width:15%;">
                                <div style="margin: 0 10% 0 10%;" class="row">
                                    <a title="Ver" style="margin: 0% 0% 0% 5%;" href="#" class="btn btn-success">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a style="margin: 0% 0% 0% 5%;" title="Editar Detalle" href="#"
                                        class="btn btn-primary">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php }  ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <a class="btn btn-success col-sm-2 mx-auto" href="#" title="Agragar hora" data-toggle="modal"
            data-target="#agregar_hora">Agregar Hora</a>
    </div>
    <br>
    <div class="row d-flex justify-content-center">

        <a class="btn btn-primary col-sm-3 mr-3" href="#" title="Agragar hora" data-toggle="modal"
            data-target="#agregar_hora">Descargar Reporte Mensual</a>

        <a class="btn btn-primary col-sm-3" href="#" title="Agragar hora" data-toggle="modal"
            data-target="#agregar_hora">Enviar Reporte Mensual</a>



    </div>
</div>
<!--====================== MODAL AGREGAR HORAS ======================-->
<div class="modal fade" id="agregar_hora" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 style="text-align:center;" class="modal-title" id="scrollmodalLabel">Agregar Hora</h4>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <form action="../../soft/actions/agregar_hora.php" method="POST">
                            <div class="form-row">
                                <div class="form-group col-sm-4 mx-auto">
                                    <label for="date" class="justify-content-center">Fecha</label>
                                    <input type="date" name="fecha" id="" class="form-control">
                                </div>
                            </div>
                            <div class="form-row d-flex justify-content-center">
                                <div class="form-group col-sm-3">
                                    <label for="horas">Horas</label>
                                    <input class="form-control" type="number" name="horas" required>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="name">Empresa</label>
                                    <select name="empresa[]" id="" class="form-control" required>
                                        <option value="">-.Elegir.-</option>
                                        <option value="taker">Taker</option>
                                        <option value="ExportService">ExportService</option>
                                        <option value="JMendez">J. Mendez y Asociados</option>
                                        <option value="Global">Global</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-6 mx-auto">
                                    <label for="name">Título:</label>
                                    <input class="form-control" type="text" name="titulo"
                                        placeholder="Descripción corta" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-6 mx-auto">
                                    <label for="name">Descripción:</label>
                                    <textarea name="descripcion" id="" cols="30" rows="10"
                                        placeholder="Detalle de la tarea" class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-3 mx-auto">
                                    <label for="name">Proyecto</label>
                                    <select name="colaborador[]" id="" class="form-control" required>
                                    <option value="">-.Elegir.-</option>
                                            <option value="Simple Solution">Simple Solution</option>
                                            <option value="Mantenimiento">Mantenimiento</option>
                                            <option value="Banners">Banners</option>
                                            <option value="Taker Turismo">Taker Turismo</option>
                                            <option value="Bolsa de Trabajo">Bolsa de Trabajo</option>
                                            <option value="Midas">Midas</option>
                                            <option value="Caleri">Caleri</option>
                                            <option value="Flota">Flotas</option>
                                            <option value="CLientes Server Group">Clientes ServerGroup</option>
                                            <option value="Minner App">Minner App</option>
                                            <option value="Otros">Otros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 mx-auto" style="text-align: center;">
                                    <button type="submit" name="cargar" id="cargar"
                                        class="btn btn-primary">Cargar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>