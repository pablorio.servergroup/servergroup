<?php

include('../../db.php');

$id = $_GET['id'];

$query = "DELETE FROM `tickets` WHERE id = '$id'";
$result = mysqli_query($conn, $query);

if (!$result) {

    $_SESSION['message'] = 'Hubo algun al eliminar el Ticket';
    $_SESSION['message_type'] = 'danger';
    header('location:../views/historial_ticket.php');

} else {

    $_SESSION['message'] = 'Ticket eliminado correctamente!';
    $_SESSION['message_type'] = 'info';
    header('location:../views/historial_ticket.php');

}
