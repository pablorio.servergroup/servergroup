<?php 

include('../../db.php'); ?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ServerGroup :: Control general de Gestion</title>
    <meta name="description" content="picadas macanudas Dashboard">
    <meta name="viewport" content="width=device-width">

    <link rel="apple-touch-icon" href="../images/avion.png">
    <link rel="shortcut icon" href="../images/avion.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="../assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
</head>

<body class="bg-dark">

    <?php
    
     if($_SESSION['permiso'] == 'pachi'){
         
        include('../fijos/pannel_right.php');
        include('../fijos_user/pannel_left_pachi.php'); 
        include('../partial_views/tabla_historial_ticket_super_user_taker_otros.php');

     }elseif($_SESSION['permiso'] == 'super_user') {

        echo 'No tenei Permiso hueeon';

     }else{

        echo 'No tenei Permiso hueeon';

     }

    ?>
    <div class="footer">
        <div class="d-flex">
            <div style="text-align:center;" class="col-sm-7 mx-auto">
                <img src="../images/avion.png" style="width: 3em;" alt="">
                <hr style="color:black; width:80%;">
                <h6> <small> Tecnología programada por </small>BUILD.IT <small> para ServerGroup</small> </h6>
                <hr style="color:black; width:80%;">

            </div>
        </div>
        <br>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="../assets/js/main.js"></script>

</body>
